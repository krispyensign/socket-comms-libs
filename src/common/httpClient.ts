import got from 'got'

export class HttpClient {

  public static async getJson<T>(url: string): Promise<T | Error> {
    let result: T | Error
    try {
      let innerResult: T | undefined = await got(url).json<T>()
      if (innerResult !== undefined) result = innerResult
      else result = new Error('Failed to get back response from url: ' + url)
    } catch (e) {
      result = e
    }
    return result
  }
}
