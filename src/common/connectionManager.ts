import { Subscriber, Push, Publisher, Pull } from 'zeromq'
import { Decoder, Encoder } from '@msgpack/msgpack'
import WebSocket from 'ws'

export interface Config {
  publicWSEndpoint: string | URL
  authWSEndpoint: string | URL
  workRequestBind: string
  workResultBind: string
  tickerBind: string
  workerEndpoint: string
  tickerEndpoint: string
  collectorEndpoint: string

}

export class ConnectionManager {
  private collectorsZ: Subscriber[] = []
  private workersZ: Push[] = []
  private tickConsumerZ?: Subscriber
  private tickDistributorZ?: Publisher
  private distributorZ?: Publisher
  private consumerZ?: Pull
  private readonly encoder = new Encoder()
  private readonly decoder = new Decoder()
  private authWS?: WebSocket
  private publicWS?: WebSocket
  private readonly conf: Config

  constructor(conf: Config) {
    this.conf = conf
  }

  public shutdown(): void {
    for (let collector of this.collectorsZ) {
      if (collector !== undefined && !collector.closed) {
        collector.disconnect(this.conf.collectorEndpoint)
        collector.close()
      }
    }

    this.workersZ?.forEach(worker => worker.close())
    this.tickConsumerZ?.disconnect(this.conf.tickerEndpoint)
    this.tickConsumerZ?.close()
    this.tickDistributorZ?.close()
    this.distributorZ?.close()
    this.consumerZ?.close()
    this.authWS?.removeAllListeners().close()
    this.publicWS?.removeAllListeners().close()
  }

  public getCollector(): Subscriber {
    let collector = new Subscriber()
    collector.connect(this.conf.collectorEndpoint)
    this.collectorsZ.push(collector)
    return collector
  }

  public getWorker(): Push {
    let worker = new Push()
    worker.connect(this.conf.workerEndpoint)
    this.workersZ.push(worker)
    return worker
  }

  public getTickConsumer(): Subscriber {
    if (this.tickConsumerZ) return this.tickConsumerZ
    this.tickConsumerZ = new Subscriber()
    this.tickConsumerZ.connect(this.conf.tickerEndpoint)
    return this.tickConsumerZ
  }

  public getTickDistributor(): Publisher {
    if (this.tickDistributorZ) return this.tickDistributorZ
    this.tickDistributorZ = new Publisher()
    this.tickDistributorZ.bind(this.conf.tickerBind)
    return this.tickDistributorZ
  }

  public getWorkResponder(): Publisher {
    if (this.distributorZ) return this.distributorZ
    this.distributorZ = new Publisher()
    this.distributorZ.bind(this.conf.workResultBind)
    return this.distributorZ
  }

  public getWorkConsumer(): Pull {
    if (this.consumerZ) return this.consumerZ
    this.consumerZ = new Pull()
    this.consumerZ.bind(this.conf.workRequestBind)
    return this.consumerZ
  }

  public getDecoder(): Decoder<unknown> {
    return this.decoder
  }

  public getEncoder(): Encoder<unknown> {
    return this.encoder
  }

  public getAuthWS(): WebSocket {
    if (this.authWS !== undefined) return this.authWS
    this.authWS = new WebSocket(this.conf.authWSEndpoint)
    if (!this.authWS) throw Error('Failed to create new web socket')
    return this.authWS
  }

  public getPublicWS(): WebSocket {
    if (this.publicWS !== undefined) return this.publicWS
    this.publicWS = new WebSocket(this.conf.publicWSEndpoint)
    if (!this.publicWS) throw Error('Failed to create new web socket')
    return this.publicWS
  }
}
