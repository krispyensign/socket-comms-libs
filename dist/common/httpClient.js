"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpClient = void 0;
const got_1 = __importDefault(require("got"));
class HttpClient {
    static async getJson(url) {
        let result;
        try {
            let innerResult = await got_1.default(url).json();
            if (innerResult !== undefined)
                result = innerResult;
            else
                result = new Error('Failed to get back response from url: ' + url);
        }
        catch (e) {
            result = e;
        }
        return result;
    }
}
exports.HttpClient = HttpClient;
//# sourceMappingURL=httpClient.js.map