export declare class HttpClient {
    static getJson<T>(url: string): Promise<T | Error>;
}
