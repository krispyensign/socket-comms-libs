import { Subscriber, Push, Publisher, Pull } from 'zeromq';
import { Decoder, Encoder } from '@msgpack/msgpack';
import WebSocket from 'ws';
export interface Config {
    publicWSEndpoint: string | URL;
    authWSEndpoint: string | URL;
    workRequestBind: string;
    workResultBind: string;
    tickerBind: string;
    workerEndpoint: string;
    tickerEndpoint: string;
    collectorEndpoint: string;
}
export declare class ConnectionManager {
    private collectorsZ;
    private workersZ;
    private tickConsumerZ?;
    private tickDistributorZ?;
    private distributorZ?;
    private consumerZ?;
    private readonly encoder;
    private readonly decoder;
    private authWS?;
    private publicWS?;
    private readonly conf;
    constructor(conf: Config);
    shutdown(): void;
    getCollector(): Subscriber;
    getWorker(): Push;
    getTickConsumer(): Subscriber;
    getTickDistributor(): Publisher;
    getWorkResponder(): Publisher;
    getWorkConsumer(): Pull;
    getDecoder(): Decoder<unknown>;
    getEncoder(): Encoder<unknown>;
    getAuthWS(): WebSocket;
    getPublicWS(): WebSocket;
}
