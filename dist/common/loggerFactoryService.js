"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoggerFactoryService = void 0;
const winston_1 = __importDefault(require("winston"));
class LoggerFactoryService {
    getLogger(serviceName) {
        let myformat = winston_1.default.format.printf((_a) => {
            var _b;
            var { level, message, timestamp } = _a, metadata = __rest(_a, ["level", "message", "timestamp"]);
            let msg = `${timestamp} [${level}] [${serviceName}] : ${message} `;
            if (metadata && !(((_b = Object.keys(metadata)) === null || _b === void 0 ? void 0 : _b.length) < 1 && metadata.constructor === Object)) {
                msg += JSON.stringify(metadata);
            }
            return msg;
        });
        let logger = winston_1.default.createLogger({
            level: 'info',
            format: winston_1.default.format.json(),
            transports: [
                new winston_1.default.transports.File({
                    filename: `logs/${serviceName}-error.log`,
                    options: {
                        level: 'error',
                    },
                }),
                new winston_1.default.transports.File({ filename: `logs/${serviceName}-combined.log` }),
            ],
        });
        if (process.env.NODE_ENV !== 'production') {
            logger.add(new winston_1.default.transports.Console({
                format: winston_1.default.format.combine(winston_1.default.format.colorize(), winston_1.default.format.timestamp(), myformat),
            }));
        }
        return logger;
    }
}
exports.LoggerFactoryService = LoggerFactoryService;
//# sourceMappingURL=loggerFactoryService.js.map