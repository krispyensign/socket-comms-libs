"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionManager = void 0;
const zeromq_1 = require("zeromq");
const msgpack_1 = require("@msgpack/msgpack");
const ws_1 = __importDefault(require("ws"));
class ConnectionManager {
    constructor(conf) {
        this.collectorsZ = [];
        this.workersZ = [];
        this.encoder = new msgpack_1.Encoder();
        this.decoder = new msgpack_1.Decoder();
        this.conf = conf;
    }
    shutdown() {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        for (let collector of this.collectorsZ) {
            if (collector !== undefined && !collector.closed) {
                collector.disconnect(this.conf.collectorEndpoint);
                collector.close();
            }
        }
        (_a = this.workersZ) === null || _a === void 0 ? void 0 : _a.forEach(worker => worker.close());
        (_b = this.tickConsumerZ) === null || _b === void 0 ? void 0 : _b.disconnect(this.conf.tickerEndpoint);
        (_c = this.tickConsumerZ) === null || _c === void 0 ? void 0 : _c.close();
        (_d = this.tickDistributorZ) === null || _d === void 0 ? void 0 : _d.close();
        (_e = this.distributorZ) === null || _e === void 0 ? void 0 : _e.close();
        (_f = this.consumerZ) === null || _f === void 0 ? void 0 : _f.close();
        (_g = this.authWS) === null || _g === void 0 ? void 0 : _g.removeAllListeners().close();
        (_h = this.publicWS) === null || _h === void 0 ? void 0 : _h.removeAllListeners().close();
    }
    getCollector() {
        let collector = new zeromq_1.Subscriber();
        collector.connect(this.conf.collectorEndpoint);
        this.collectorsZ.push(collector);
        return collector;
    }
    getWorker() {
        let worker = new zeromq_1.Push();
        worker.connect(this.conf.workerEndpoint);
        this.workersZ.push(worker);
        return worker;
    }
    getTickConsumer() {
        if (this.tickConsumerZ)
            return this.tickConsumerZ;
        this.tickConsumerZ = new zeromq_1.Subscriber();
        this.tickConsumerZ.connect(this.conf.tickerEndpoint);
        return this.tickConsumerZ;
    }
    getTickDistributor() {
        if (this.tickDistributorZ)
            return this.tickDistributorZ;
        this.tickDistributorZ = new zeromq_1.Publisher();
        this.tickDistributorZ.bind(this.conf.tickerBind);
        return this.tickDistributorZ;
    }
    getWorkResponder() {
        if (this.distributorZ)
            return this.distributorZ;
        this.distributorZ = new zeromq_1.Publisher();
        this.distributorZ.bind(this.conf.workResultBind);
        return this.distributorZ;
    }
    getWorkConsumer() {
        if (this.consumerZ)
            return this.consumerZ;
        this.consumerZ = new zeromq_1.Pull();
        this.consumerZ.bind(this.conf.workRequestBind);
        return this.consumerZ;
    }
    getDecoder() {
        return this.decoder;
    }
    getEncoder() {
        return this.encoder;
    }
    getAuthWS() {
        if (this.authWS !== undefined)
            return this.authWS;
        this.authWS = new ws_1.default(this.conf.authWSEndpoint);
        if (!this.authWS)
            throw Error('Failed to create new web socket');
        return this.authWS;
    }
    getPublicWS() {
        if (this.publicWS !== undefined)
            return this.publicWS;
        this.publicWS = new ws_1.default(this.conf.publicWSEndpoint);
        if (!this.publicWS)
            throw Error('Failed to create new web socket');
        return this.publicWS;
    }
}
exports.ConnectionManager = ConnectionManager;
//# sourceMappingURL=connectionManager.js.map