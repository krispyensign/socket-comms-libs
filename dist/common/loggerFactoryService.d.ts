import { Logger } from 'winston';
export declare class LoggerFactoryService {
    getLogger(serviceName: string): Logger;
}
